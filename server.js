//express, body-parser, sequelize

const express = require("express");
const bodyParser = require('body-parser');
const sequelize = require('sequelize');

//crearea variabilei de tip Express si parsarea in JSON
const app = express();
app.use(bodyParser.json());

//initializare baze de date 
const connection = new sequelize("Masinutze", "root", "",{
    dialect: "mysql"
})

//creare tabele
const Car = connection.define('Car', {
    marca: sequelize.STRING,
    cp: sequelize.INTEGER,
    culoare: sequelize.STRING,
    capacitateMotor: sequelize.INTEGER
})

const Showroom = connection.define('showroom', {
    denumire: sequelize.STRING,
    strada: sequelize.STRING,
    marca: sequelize.STRING
})

//creare legatura intre tabele
Showroom.hasMany(Car, {onDelete: "Cascade", hooks: true})

//resetare baza de date
app.get("/reset", async (req, res)=>{
    try{
        await connection.sync({force: true});
        res.status(200).send({message: "Miau"});
    }catch(err){
        console.log(err);
        res.status(500).send({message: "Ham"})
    }
})

//inserare masina
app.post("/car", async(req, res)=>{
    const car = {
        marca: req.body.marca,
        cp: req.body.cp,
        culoare: req.body.culoare,
        capacitateMotor: req.body.capacitateMotor,
        showroomId: ""
    }

    try{
        const showroom = await Showroom.findOne({where: {denumire: req.body.denumire}})
        car.showroomId = showroom.id;
        console.log(car.showroomId)
        await Car.create(car);
        res.status(200).send({message: "Bravo"})
    }catch(e){
        console.log(e);
        res.status(500).send({message: "iiihh"})
    }
})

//inserare reprezentante
app.post("/showroom", async(req, res)=>{
    const showroom = {
        denumire: req.body.denumire,
        strada: req.body.strada,
        marca: req.body.marca
    }

    try{
        await Showroom.create(showroom)
        res.status(200).send({message: "nu mai stiu"})

    }catch(e){
        console.error(e);
        res.status(500).send({message: "Cici"})
    }
})

//afisare masini
app.get("/car", async(req,res)=>{
    try{
        const car = await Car.findOne({where:{id: 1}});
        console.log(car)
        res.status(200).send({message: "Aiti", car})
    }catch(e){
        console.log(e)
        res.status(500).send({message: "Bujie"})
    }
})

//afisare reprezentanta si masinile asociate
app.get("/showroomCars", async(req,res)=>{
    try{
        const showroom = await Showroom.findAll({
            include: [{model: Car}]
        });
        console.log(showroom)
        res.status(200).send( showroom ) 
    }catch(e){
        console.error(e)
        res.status(500).send({message: "FRagomir"})
    }
})



















//deschiderea server
app.listen(8080, 'localhost',()=>{
    console.log("Server started on 8080");
})